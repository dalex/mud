-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-07-30 05:08:30
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mud`
--

-- --------------------------------------------------------

--
-- 表的结构 `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `hp` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `courage` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defence` int(11) NOT NULL,
  `celerity` int(11) NOT NULL,
  `d_hp` double NOT NULL,
  `d_exp` double NOT NULL,
  `d_courage` double NOT NULL,
  `d_attack` double NOT NULL,
  `d_defence` double NOT NULL,
  `d_celerity` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `job`
--

INSERT INTO `job` (`id`, `name`, `hp`, `exp`, `courage`, `attack`, `defence`, `celerity`, `d_hp`, `d_exp`, `d_courage`, `d_attack`, `d_defence`, `d_celerity`) VALUES
(1, 'xxb', 150, 200, 150, 700, 500, 3, 0.11, 0.15, 0.15, 0.2, 0.13, 0.05),
(2, 'sk', 5000, 150, 1000, 600, 600, 1, 0.01, 0.2, 0.5, 0.02, 0.2, 0.01);

-- --------------------------------------------------------

--
-- 表的结构 `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL COMMENT 'user id',
  `sex` int(11) NOT NULL COMMENT '1 for man and 0 for woman',
  `job` int(11) NOT NULL,
  `hp` int(11) NOT NULL COMMENT '100-10000',
  `courage` int(11) NOT NULL COMMENT '100-10000',
  `level` int(11) NOT NULL COMMENT '1-99',
  `exp` int(11) NOT NULL,
  `bag` text NOT NULL COMMENT 'items list like (1,2,3)',
  `equipment` text NOT NULL COMMENT '(arm,clothes,shoes)',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `player`
--

INSERT INTO `player` (`id`, `user`, `sex`, `job`, `hp`, `courage`, `level`, `exp`, `bag`, `equipment`) VALUES
(1, 1, 1, 1, 0, 15, 3, 25, '()', '()'),
(2, 2, 0, 1, 200, 200, 3, 10, '()', '()');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 NOT NULL,
  `nickname` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nickname`) VALUES
(1, 'dalex', '123456', 'xxb'),
(2, 'sam', 'haha', 'sk'),

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
