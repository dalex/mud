package dalex.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class OnlineClientIOCache {
	/*
	 * 在线客户端IOCache .
	 */
	private ObjectInputStream ois; 
	private ObjectOutputStream oos;
	
	public OnlineClientIOCache(ObjectInputStream ois, ObjectOutputStream oos) {
		this.ois = ois;
		this.oos = oos;
	}
	
	public ObjectInputStream getOis() {
		return ois;
	}
	
	public ObjectOutputStream getOos() {
		return oos;
	}
}
