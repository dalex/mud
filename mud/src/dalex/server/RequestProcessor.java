package dalex.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import dalex.command.*;
import dalex.player.*;

import java.sql.*;

public class RequestProcessor extends Thread{

	private Socket socket ; 
	private int socketID ; 
	private ObjectInputStream in ; 
	private ObjectOutputStream out ; 
	private Connection conn;
	private Player player;
	
	public RequestProcessor(Socket socket){
		this.socket = socket ; 
		try{
			in = new ObjectInputStream( socket.getInputStream());
			out = new ObjectOutputStream( socket.getOutputStream() ) ;
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void run(){

		try{
			socketID = Common.onlineIOCacheCounter++ ; 
			
			/****** 请求处理过程 ******/
			while(true){
				Command command = (Command) in.readObject() ; 
				System.out.println("received command :"+command.getCommand()) ; 
				
				command.setPlayer(Common.onlinePlayerMap.get(command.nickname));
				command.setStatement(Common.statement) ; 
				command.setObjectOutputStream(out);
				command.setObjectInputStream(in);
				command.action();
				
				System.out.println("命令："+command.getCommand()+"执行完毕") ;
			}	
			
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		} catch ( ClassNotFoundException ioe ){
			ioe.printStackTrace();
		}
	}
}
