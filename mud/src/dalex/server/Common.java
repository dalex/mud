package dalex.server;

import java.net.ServerSocket;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import dalex.world.World;


import dalex.player.Player;

public class Common {
	public static ServerSocket serverSocket ; 
	/**********服务器套接字**************/
		
	public static Statement statement ;  
	/********** Statement ***********/
	
	public static ResultSet resultSet ; 
	/************* resultSet*********/
	
	public static int onlineIOCacheCounter ; 
	/**********在线客户端计数器**************/
	
	public static int onlinePlayerCounter ; 
	/**********在线玩家计数器**************/
	
	public static final int SERVER_PORT = 7890 ;
	/**********服务器端口**************/
	
	public static Map<String, OnlineClientIOCache> onlineClientIOCacheMap;
	/**********在线客户端IO容器**************/

	public static Map<String,Player> onlinePlayerMap;
	/**********在线玩家容器**************/
	
	public static World world;
	/**********游戏世界地图**************/
	
	static{/**********容器初始化**************/
		onlineClientIOCacheMap = new HashMap<String,OnlineClientIOCache>();
		onlinePlayerMap = new HashMap<String, Player>();
	}
	
}
