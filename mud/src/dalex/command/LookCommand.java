package dalex.command;

import java.io.IOException;

import dalex.server.Common;
import dalex.world.*;

public class LookCommand extends Command{
	public static final String name = "Look";
	
	public void action(){
		//change the position
		try{
			Position tempPos = player.getPosition();
			
			Map tempMap = Common.world.getMap(tempPos);
			if(tempMap == null){
				out.writeObject("没有该地图，请联系管理员！");
				return;
			}
			
			
			Site tempSite = tempMap.getSite(tempPos);
			if(tempSite == null){
				out.writeObject("没有该地点，请联系管理员！");
				return;
			}
			
			if(tempSite.getDescription() != null)
				out.writeObject(tempSite.getDescription());
			String txt = "少侠现在在"+tempSite.getName()+",我们明显看到";
			
			
			if((tempSite = tempMap.getSite(
					new Position(tempPos.getX(),tempPos.getY()+1,
							tempPos.getMapID()))) != null){
				txt = txt + " 向北是" + tempSite.getName();
			}
			if((tempSite = tempMap.getSite(
					new Position(tempPos.getX(),tempPos.getY()+1,
							tempPos.getMapID()))) != null){
				txt = txt + " 向南是" + tempSite.getName();
			}
			if((tempSite = tempMap.getSite(
					new Position(tempPos.getX()-1,tempPos.getY(),
							tempPos.getMapID()))) != null){
				txt = txt + " 向西是" + tempSite.getName();
			}
			if((tempSite = tempMap.getSite(
					new Position(tempPos.getX()+1,tempPos.getY(),
							tempPos.getMapID()))) != null){
				txt = txt + " 向东是" + tempSite.getName();
			}
			
			out.writeObject(txt);
			
			
		}catch(IOException e){
			e.printStackTrace();
		}	
		

	}
}