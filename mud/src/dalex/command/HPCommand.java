package dalex.command;

import java.io.IOException;

import dalex.player.*;

public class HPCommand extends Command{
	public final String name = "hp";
	
	public void action(){
		try{
			out.writeObject("hp " + player.getHP() + "   courage " +player.getCourage() );
			out.writeObject("level " + player.getLevel() + "  exp " +player.getEXP());
			out.writeObject("END");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
