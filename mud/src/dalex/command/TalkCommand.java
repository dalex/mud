package dalex.command;

import java.io.IOException;
import java.io.ObjectOutputStream;

import dalex.server.Common;

public class TalkCommand extends Command{
	private static final long serialVersionUID = 1L;

	public void action() {
		//命令格式： talkTo dalex 你好啊啊啊啊
		if( this.command.split(" ").length < 3 ){
			try {
				this.out.writeObject("命令格式错误");
				this.out.writeObject("END") ; 
				System.out.println("命令格式错误") ;
			} catch (IOException e) {
				e.printStackTrace();
			}		
			return ; 
		}
		String toUser = this.command.split(" ")[1] ; 
		
		String content = (String) this.command.subSequence(toUser.length()+7 , this.command.length()) ; 
		
		System.out.println("toUser:"+toUser) ; 
		System.out.println("content:"+content) ; 
		
		try {
			if ( toUser.equals(this.nickname) ){
				this.out.writeObject("不要自言自语");
				this.out.flush() ; 
			}else if( Common.onlineClientIOCacheMap.containsKey(toUser) ){
				ObjectOutputStream oos = Common.onlineClientIOCacheMap.get(toUser).getOos() ; 
				oos.writeObject(this.nickname+" 对你说:"+content) ;
				oos.flush() ;
				
				this.out.writeObject("发送成功");
				this.out.flush() ; 			
			}else{
				this.out.writeObject("该用户未在线或不存在");
				this.out.flush(); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
