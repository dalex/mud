package dalex.command;

import java.io.IOException;
import java.sql.*;
import java.util.Random;

import dalex.player.Player;
import dalex.server.Common;

public class FightCommand extends Command{
	public static final String name = "fight";
	
	public void action(){
		String[] strArray = command.split(" ");
		String target;
		
		try{
			if(strArray.length >= 2){
				target = strArray[1];
				
				ResultSet rs = statement.executeQuery("SELECT * FROM user WHERE nickname = '" + target + "'");
				
				if(rs.next()){
					rs = statement.executeQuery("SELECT * FROM player WHERE user = " + rs.getInt("id"));
				
				
					if(rs.next()){
					
						out.writeObject("is fighting with " + strArray[1]);
						Player p1 =  player;
						Player p2 =  new Player(Common.statement,rs.getInt("id"));
						
						//figth
						
						if(p1.getID() == p2.getID()){
							out.writeObject("不能跟自己战斗");
							return;
						}
						
						if(dalex.server.Common.onlinePlayerMap.get(p2.getID())==null){
							out.writeObject("该玩家不在线~");
							return;
						}
						
						if(!p1.getPosition().equals(p1.getPosition())){
							out.writeObject("此地没有该玩家");
							return;
						}
						
						//who frist
						int mark = 0;
						Player temp;
						Random random = new Random();
						mark = random.nextInt()%(p1.getCelerity() + p2.getCelerity()) + 1;
						if(p1.getCelerity() >= mark){
							
						}else{
							temp = p2;
							p2 = p1;
							p1 = temp;
						}
						
						boolean flag = false;	//记录是否有进入战斗状态
						
						while(p1.getCourage() > 0 && p1.getHP() > 0 
								&& p2.getCourage() > 0 && p2.getHP() > 0){
							//p1 attack p2
							flag = true;
							
							out.writeObject(p1.getName() + "对" + p2.getName() + "发起了进攻");
							out.writeObject(p1.getName() + "使出了一记重拳！");
							mark = random.nextInt() % 100 + 1;
							if(mark < p2.getCelerity()){
								out.writeObject(p2.getName() + "轻松躲开了！");
							}else{
								if(p1.getAttack() - p2.getDefence() > 0){
									out.writeObject(p2.getName() + "吃了" + p1.getName() + "一拳，看上去很痛的样子");
									if(p2.getHP()-(p1.getAttack() - p2.getDefence()) > 0 &&
											p2.getCourage()-p1.getCourage()/(double)(p2.getCourage() + p1.getCourage()) * 
											(p1.getAttack() - p2.getDefence()) > 0
											){
										p2.setPlayer(p2.getHP()-(p1.getAttack() - p2.getDefence()), 
												p2.getEXP(), 
												(int)(p2.getCourage()-p1.getCourage()/(double)(p2.getCourage() + p1.getCourage()) * 
												(p1.getAttack() - p2.getDefence())));
									}else{
										if(p2.getHP()-(p1.getAttack() - p2.getDefence()) <= 0){
											out.writeObject(p2.getName() + "受重伤，倒地不起！");
											p2.setPlayer(p2.getHP()-(p1.getAttack() - p2.getDefence()), 
													p2.getEXP(), 
													(int)(p2.getCourage()-p1.getCourage()/(double)(p2.getCourage() + p1.getCourage()) * 
													(p1.getAttack() - p2.getDefence())));
										}else if(p2.getCourage()-p1.getCourage()/(double)(p2.getCourage() + p1.getCourage()) * 
												(p1.getAttack() - p2.getDefence()) <= 0){
											out.writeObject(p2.getName() + "转身就跑！");
											p2.setPlayer(p2.getHP()-(p1.getAttack() - p2.getDefence()), 
													p2.getEXP(), 
													(int)(p2.getCourage()-p1.getCourage()/(double)(p2.getCourage() + p1.getCourage()) * 
													(p1.getAttack() - p2.getDefence())));
										}
										break;
									}
								}else{
									out.writeObject(p2.getName() + "吃了" + p1.getName() + "一拳，感觉像是在挠痒！");
								}
							}
							temp = p2;
							p2 = p1;
							p1 = temp;
							System.out.println();
							
							//延时
							Thread.sleep(2000);
						}	
						
						if(flag){
							int exp;
							if(p1.getLevel() > p2.getLevel()){
								exp = (int)(p1.getUPEXP() / 50.0 * 
								( (p1.getLevel() - p2.getLevel()) / 100.0));
							}else{
								exp = (int)(p1.getUPEXP() / 50.0 * 
										( 1 - (p2.getLevel() - p1.getLevel()) / 100.0));
							}
							p1.setPlayer(p1.getHP(), p1.getEXP() + exp, p1.getCourage());
							p1.update();
							out.writeObject(p1.getName() + "赢得游戏！");
						}else{
							out.writeObject("不符合战斗条件！");
						}
						
					}
				}else{
					out.writeObject("不存在" + target);
				}
			}else{
				out.writeObject("提示：fight targatID");
				

			}
			out.writeObject("END");
			out.flush();
		}catch(IOException e){
			e.printStackTrace();
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}

		
		//find whether the target is in the place 
	}
}
