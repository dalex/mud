package dalex.command;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import dalex.player.Player;
import dalex.server.Common;
import dalex.server.OnlineClientIOCache;

public class LoginCommand extends Command{
	/*
	 * 登录验证
	 */
	private static final long serialVersionUID = 1L;
	public final String name = "login";
	
	public void action(){
		
		if( this.command.split(" ").length < 3){
			try {
				this.out.writeObject("用户名或密码不能为空");
				this.out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return ; 
		}
		
		String account = this.command.split(" ")[1] ; 
		String passwd = this.command.split(" ")[2] ; 		
		String nickname =""; 
		
		try{			
			ResultSet rs = this.statement.executeQuery("select password,nickname from user where username='"+account+"'") ; 			
			System.out.println("username:"+account) ; 
			System.out.println("passwd:"+passwd) ; 
			
			if( rs.next() ){
				if( rs.getString(1).equals(passwd) ){
					/****验证通过****/
					System.out.println("验证通过") ;
					nickname = rs.getString(2) ; 
					
					BroadcastCommand bc = new BroadcastCommand() ; 
					bc.setCommand("broadcast "+nickname+"上线了");
					bc.action();
					
					this.out.writeObject(new String("LOGIN_SUCCEEDED "+nickname));
					this.out.flush(); 
					System.out.println("sent to client successfully") ; 
					
					
					/* 
					 * 验证通过后，将当前socket保存至 OnlineClientIOCache表中
					 * 加载Player ,通过数据库查询 ，  如果存在 ，直接加载 ，如果不存在 ，进入创建过程 。
					 * 加载完成后 ，将player对象保存至OnlinePlayerMap 中。	
					 * 				
					 */
					this.nickname = nickname ; 
					Common.onlineClientIOCacheMap.put(nickname, new OnlineClientIOCache(this.in, this.out)) ;
					loadPlayer(nickname) ; 
					Common.onlinePlayerMap.put(nickname, this.player) ; 
					
				}else{
					/****密码错误****/
					System.out.println("密码错误") ;
					this.out.writeObject(new String("LOGIN_FAILED"));
					this.out.flush(); 
				}
			}else{
				/****用户不存在****/
				System.out.println("用户不存在") ;
				this.out.writeObject(new String("LOGIN_FAILED"));
				this.out.flush(); 
			}
			//rs.close();  
		}catch(Exception e ){
			e.printStackTrace();
		}
		
	}
	
	/****************** 加载 Player *********************/ 
	void loadPlayer(String nickname){
		//存在时加载 、不存在时创建 。
		String sql1 = "select id from user where nickname='"+nickname+"'" ;
		try {
			ResultSet rs = this.statement.executeQuery(sql1) ;
			int id = 0 ; 
			if(rs.next()){
				id = rs.getInt("id") ;
				System.out.println("查询问结果：id="+id) ; 
			}else{
				System.out.println("user not found") ; 
			}

			String sql2 = "select * from player where user = '"+id+"'" ; 
			rs = this.statement.executeQuery(sql2) ; 
			if ( rs.next() ){
				//player存在
				this.player = new Player(this.statement , id) ;
				this.out.writeObject("已成功加载Player信息");				
				//TODO 显示当前用户所在位置 ，以及相应场景描述				
				this.out.flush();
			}else{
				//player不存在
				createPlayer() ; 			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch( IOException ioe){
			ioe.printStackTrace();
		}
	}
	
	/**************** 创建player向导 *******************/
	void createPlayer(){
		int profession ; 
		int sex ; 
		
		//创建、并将结果写入数据库
		try {
			this.out.writeObject("欢迎进入MUD世界 ， 在正式开始之前，请先创建您的角色 ：");
			this.out.writeObject("选择你的职业：1.xxb  2.sk (输入数字进行选择)");
			this.out.flush(); 
			
			while(true){
				int ch = this.in.readInt() ; 
				if( ch == 1 || ch == 2){
					profession = ch ; 
					break ; 
				}
				this.out.writeObject("输入有误，请重新输入 ") ;
				this.out.flush() ; 
			}
			
			this.out.writeObject("请选择性别 ：0：女   1：男 （输入数字进行选择）");
			this.out.flush() ; 
			while(true){
				int ch = this.in.readInt() ; 
				if( ch == 1 || ch == 2){
					sex = ch ; 
					break ; 
				}
				this.out.writeObject("输入有误，请重新输入 ") ;
				this.out.flush() ; 
			}
			
			String sql1 = "select id from user where nickname='"+this.nickname+"'" ; 
			System.out.println("查询语句："+sql1) ; 
			
			int id = 999999 ; 
			int hp = 0 ,courage = 0 ; 
			try {
				ResultSet rs = this.statement.executeQuery(sql1) ;
				if( rs.next() ){
					id = rs.getInt("id") ; 
					
					String sql2 = "select hp,courage from job where id='"+profession+"'" ;
					ResultSet rs2 = this.statement.executeQuery(sql2) ; 
					if( rs2.next() ){
						hp = rs2.getInt("hp") ; 
						courage = rs2.getInt("courage") ; 
					}else{
						System.out.println("profession not found") ; 
						return ; 
					}
				}else{
					System.out.println("user not found") ;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			
			String sql3 = "insert into player (user,sex,job,hp,courage,level,exp,bag,equipment) values("+id+","+sex+","+profession+","+hp+","+courage+",0,0,'()','()')" ; 
			System.out.println(sql3) ; 
			try {
				this.statement.execute(sql3) ;
				this.out.writeObject("人物创建成功") ; 
				this.out.writeObject("欢迎正式进入MUD的世界") ; 
				System.out.println("创建人物信息已写入Player数据表") ; 
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			
			this.player = new Player(this.statement , id );	
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}





