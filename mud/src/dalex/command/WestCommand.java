package dalex.command;

import java.io.IOException;
import java.sql.Statement;

import dalex.server.Common;
import dalex.world.Map;
import dalex.world.Position;
import dalex.world.Site;

public class WestCommand extends Command{
	public static final String name = "west";
	
	public void action(){
		//change the position
		try{
			Position tempPos = player.getPosition();
			
			Map tempMap = Common.world.getMap(tempPos);
			if(tempMap == null){
				out.writeObject("没有该地图，请联系管理员！");
				return;
			}
			
			
			Site tempSite = tempMap.getSite(tempPos);
			if(tempSite == null){
				out.writeObject("没有该地点，请联系管理员！");
				return;
			}
			if((tempSite = tempMap.getSite(
					new Position(tempPos.getX()-1,tempPos.getY(),
							tempPos.getMapID()))) != null){
				player.setPosition(new Position(tempPos.getX()-1,tempPos.getY(),tempPos.getMapID()));
			}else{
				out.writeObject("不好意思，没有路！");
			}
			
			LookCommand lookCommand = new LookCommand();
			lookCommand.setPlayer(player);
			lookCommand.setObjectOutputStream(out);
			lookCommand.action();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}