package dalex.command;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.sql.Statement;

import dalex.player.Player;

public abstract class Command implements Serializable{
	private static final long serialVersionUID = 1L;
	public final String name = "command";
	public String command = "";
	public Statement statement;
	public Player player ; 
	public String nickname ;
	public ObjectOutputStream out;
	public ObjectInputStream in ; 
	public Socket socket ; 
	
	public abstract void action();
	
	public void setCommand(String str){
		command = str;
	}
	public void setNickname(String nickname){
		this.nickname = nickname ; 
	}
	public String getCommand(){
		return command ; 
	}
	public String getName(){	//返回命令名称
		return this.name ; 
	}
	public void setStatement(Statement statement){
		this.statement = statement ; 
	}

	public void setObjectOutputStream(ObjectOutputStream out){
		this.out = out ; 
	}
	public void setObjectInputStream(ObjectInputStream in){
		this.in = in ; 
	}

	public void setPlayer(Player player) {
		this.player = player ; 
	}
	
}