package dalex.command;

import java.io.IOException;
import java.io.ObjectOutputStream;

import dalex.server.Common;

public class BroadcastCommand extends Command{

	private static final long serialVersionUID = 1L;

	public void action() {		

		try {
			if ( Common.onlineClientIOCacheMap.isEmpty() ){
				// some bugs .
				/*
				this.out.writeObject("当前整个世界上只有你一个人");
				this.out.flush() ;
				*/ 
			}else {
				for( String key : Common.onlineClientIOCacheMap.keySet() ){
					if( !key.equals(this.nickname)){
						ObjectOutputStream out = Common.onlineClientIOCacheMap.get(key).getOos() ;
						String content = this.command.split(" ")[1] ; 
						if(this.nickname == null ){
							this.nickname = "SERVER" ; 
						}
						out.writeObject(this.nickname+" 对全世界说 :"+content);
						out.flush() ;
					}
				}				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

}
