package dalex.command;

import java.io.IOException;

public class LogoutCommand extends Command{
	private static final long serialVersionUID = 1L;
	public final String name = "logout" ; 
	
	public void action(){
		try {
			this.socket.close() ;
		} catch (IOException e) {
			e.printStackTrace();
		} 		
	}
}
