package dalex.player;
import java.sql.*;
import java.io.IOException;
import java.util.*;

import dalex.server.Common;
import dalex.world.Position;

public class Player { 
	private Statement statement;
	
	private int id;
	private String name;
	private int user;
	private int sex;
	private int job;
	private String job_name;
	private int level;
	private int exp;
	private int up_exp;
	private int hp;
	private int up_hp;
	private int courage;
	private int up_courage;
	private int attack;
	private int defence;
	private int celerity;
	private Position pos;
	
	public Player(Statement statement, int userID){
		this.statement = statement;
		this.user = userID;
		
		try{
			ResultSet result = statement.executeQuery("select * from player where user = " + user);
			
			if(result.next()){
				System.out.println("player记录查询成功！");
				id = result.getInt("id");
				user = result.getInt("user");
				sex = result.getInt("sex");
				job = result.getInt("job");
				level = result.getInt("level");
				exp = result.getInt("exp");
				hp = result.getInt("hp");
				courage = result.getInt("courage");
				
				//begin to init pos
				String position = result.getString("position");
				int[] tempPos = new int[3];
				int i = 0;
				for(String x : position.split(",")){
					tempPos[i] = Integer.parseInt(x);
					i++;
				}
				pos = new Position(tempPos[0],tempPos[1],tempPos[2]);
				//end
				
			}
			result = statement.executeQuery("select * from job where id = " + job);
			
			if(result.next()){
				System.out.println("job记录查询成功！");
				up_hp = (int)(result.getInt("hp") * 
						Math.pow((1 + result.getDouble("d_hp")),level - 1));
				up_exp = (int)(result.getInt("exp") * 
						Math.pow((1 + result.getDouble("d_exp")),level - 1));
				up_courage = (int)(result.getInt("courage") * 
						Math.pow((1 + result.getDouble("d_courage")),level - 1));
				attack = (int)(result.getInt("attack") * 
						Math.pow((1 + result.getDouble("d_attack")),level - 1));
				defence = (int)(result.getInt("defence") * 
						Math.pow((1 + result.getDouble("d_defence")),level - 1));
				celerity = (int)(result.getInt("celerity") * 
						Math.pow((1 + result.getDouble("d_celerity")),level - 1));
				job_name = result.getString("name");
			}
			
			result = statement.executeQuery("select * from user where id = " + user);
			
			if(result.next()){
				name = result.getString("nickname");
				name = name + "(" + result.getString("username") + ")";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void update(){
		try{
			ResultSet result = statement.executeQuery("select * from job where id = " + job);
			
			if(result.next()){
				System.out.println("job记录查询成功！");
				up_hp = (int)(result.getInt("hp") * 
						Math.pow((1 + result.getDouble("d_hp")),level - 1));
				up_exp = (int)(result.getInt("exp") * 
						Math.pow((1 + result.getDouble("d_exp")),level - 1));
				up_courage = (int)(result.getInt("courage") * 
						Math.pow((1 + result.getDouble("d_courage")),level - 1));
				attack = (int)(result.getInt("attack") * 
						Math.pow((1 + result.getDouble("d_attack")),level - 1));
				defence = (int)(result.getInt("defence") * 
						Math.pow((1 + result.getDouble("d_defence")),level - 1));
				celerity = (int)(result.getInt("celerity") * 
						Math.pow((1 + result.getDouble("d_celerity")),level - 1));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void setPosition(Position pos){
		this.pos = pos;
		try{
			String txt = "" + pos.getX() + "," + pos.getY() + "," + pos.getMapID();
			statement.execute("UPDATE player SET position = '" + txt + "' WHERE id = " + id);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public Position getPosition(){
		return pos;
	}
	
	public void setPlayer(int hp,int exp,int courage){
		if(hp < this.up_hp)
			this.hp = hp;
		if(hp < 0)
			this.hp = 0;
		if(exp < this.up_exp)
			this.exp = exp;
		if(exp < 0)
			this.exp = 0;
		if(courage < this.up_courage)
			this.courage = courage;
		if(courage < 0)
			this.courage = 0;
		
		if(exp > up_exp && this.level < 100){
			this.level ++;
		}
		
		try{
			statement.execute("UPDATE player SET hp = " + this.hp + 
					" ,exp = " + this.exp + " ,level = " +this.level + 
					" ,courage = " + this.courage + " WHERE id =" + id); 
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
	public int getUser(){
		return user;
	}
	
	public int getID(){
		return id;
	}
	
	public int getSex(){
		return sex;
	}
	
	public int getJob(){
		return job;
	}
	
	public int getLevel(){
		return level;
	}
	
	
	public int getEXP(){
		return exp;
	}
	
	public int getUPEXP(){
		return up_exp;
	}
	
	public int getHP(){
		return hp;
	}

	public int getUPHP(){
		return up_hp;
	}
	
	public int getCourage(){
		return courage;
	}

	public int getUPCourage(){
		return up_courage;
	}
	
	public int getAttack(){
		return attack;
	}
	
	public int getDefence(){
		return defence;
	}
	
	public int getCelerity(){
		return celerity;
	}

	public String getName(){
		return name;
	}
}
