package dalex.client;

import java.io.IOException;
import java.io.ObjectInputStream;

public class ResponseProcessor extends Thread{
	
	ObjectInputStream in ; 
	public ResponseProcessor(){
		try {
			in = new ObjectInputStream( Common.socket.getInputStream()) ;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void run(){
		
		while( Common.socket.isConnected() ){
			try {
				
				String content = (String) in.readObject() ;
				System.out.println("Received :"+content) ; 
				System.out.println("head:"+content.split(" ")[0]) ; 
				
				if( content.split(" ")[0].equals("LOGIN_SUCCEEDED")){
					//登录验证成功
					System.out.println("验证已通过 ") ;
					Common.nickname = content.split(" ")[1] ; 
					Common.loginFrame.dispose() ;
					new ClientFrame() ; 
				}else if(content.split(" ")[0].equals("LOGIN_FAILED")){
					//登录验证失败
					System.out.println("验证失败") ; 
				}else if( content.split(" ")[0].equals("REGISTE_SUCCEEDED") ){
					//注册成功
					System.out.println("注册成功 ") ;
					Common.registeFrame.dispose(); 
					Common.loginFrame = new LoginFrame() ; 
					continue ; 
				}else if( content.split(" ")[0].equals("REGISTE_USERNAME_EXIST")) {
					//注册用户名被占用
					System.out.println("用户名被占用") ; 
					continue ; 
				}else if ( content.split(" ")[0].equals("REGISTE_NICKNAME_EXIST")){
					//昵称被占用
					System.out.println("昵称被占用") ; 
					continue ; 
				}
				/*************此处可扩展*************/
				else {
					//其他情况
					if( Common.textArea == null ){
						System.out.println("FROM SERVER:"+content) ; 
					}else{
						Common.textArea.append(content+"\n\n");
					}
				}
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
