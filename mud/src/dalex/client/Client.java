package dalex.client;

import java.net.Socket;
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Client{
	
	public Client(){
		try{
			Common.socket = new Socket(Common.SERVER,Common.SERVER_PORT);
			Common.out = new ObjectOutputStream( Common.socket.getOutputStream()) ; 
			
			new ResponseProcessor().start(); 
			
			Common.commandProxy = new CommandProxy() ; 
			Common.loginFrame = new LoginFrame() ; 
			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		
		
	}
		
	public static void main(String[] args){
		new Client() ;
	}
}
