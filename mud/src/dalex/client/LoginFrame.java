package dalex.client;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class LoginFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JTextField idTxt;
	private JPasswordField pwdFld;
	
	public LoginFrame(){
		initComp() ; 
	}
	
	private void initComp(){
		this.setTitle("DALEX_MUD");
		this.setSize(330, 180);

		int x = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		int y = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		this.setLocation((x - this.getWidth()) / 2, (y-this.getHeight())/ 2);
		this.setResizable(false);
		
		JPanel mainPanel = new JPanel();
		Border border = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		mainPanel.setBorder(BorderFactory.createTitledBorder(border, "输入登录信息", TitledBorder.CENTER,TitledBorder.TOP));
		this.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(null);
		
		JLabel nameLbl = new JLabel("账号:");
		nameLbl.setBounds(50, 30, 40, 22);
		mainPanel.add(nameLbl);
		idTxt = new JTextField();
		idTxt.setBounds(95, 30, 150, 22);
		idTxt.requestFocusInWindow();
		mainPanel.add(idTxt);
		
		JLabel pwdLbl = new JLabel("密码:");
		pwdLbl.setBounds(50, 60, 40, 22);
		mainPanel.add(pwdLbl);
		pwdFld = new JPasswordField();
		pwdFld.setBounds(95, 60, 150, 22);
		mainPanel.add(pwdFld);
		
		JPanel btnPanel = new JPanel();
		this.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new BorderLayout());
		btnPanel.setBorder(new EmptyBorder(2, 8, 4, 8)); 

		JButton registeBtn = new JButton("注册");
		btnPanel.add(registeBtn, BorderLayout.WEST);
		JButton submitBtn = new JButton("登录");
		btnPanel.add(submitBtn, BorderLayout.EAST);
		
		registeBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Common.registeFrame = new RegisteFrame(); //注册
				LoginFrame.this.dispose() ; 
			}
		});
		
		submitBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//**************************************************
	public void login(){
		StringBuilder sb = new StringBuilder("login ") ;
		sb.append(idTxt.getText())
			.append(" ")
			.append(pwdFld.getPassword()) ;

		Common.commandProxy.send( sb.toString() ) ;
		
	}
}
