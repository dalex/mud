package dalex.client;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.Socket;

import javax.swing.*;


public class RegisteFrame extends JFrame {
	
	private JTextField username ; 
	private JTextField nickname;
	private JPasswordField pwdFld;
	private JPasswordField pwd2Fld;
			
	private JButton ok;
	
	public RegisteFrame(){
		initComponents() ; 
	}
	
	void initComponents(){
		this.setTitle("MUD_REGISTE");//���ñ���
		this.setSize(310, 250);

		int x = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		int y = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		this.setLocation((x - this.getWidth()) / 2, (y-this.getHeight())/ 2);
		getContentPane().setLayout(null);
		this.setResizable(false);

		JLabel label = new JLabel("username：");
		label.setBounds(24, 36, 65, 17);
		getContentPane().add(label);
		username = new JTextField(); 
		username.setBounds(110, 34, 110, 22);
		getContentPane().add(username);
		
		JLabel label2 = new JLabel("nickname:");
		label2.setBounds(24, 72, 65, 17);
		getContentPane().add(label2);
		nickname = new JTextField(); 
		nickname.setBounds(110, 70, 110, 22);
		getContentPane().add(nickname);
		
				
		JLabel label3 = new JLabel("password:*");
		label3.setBounds(24, 107, 65, 17);
		getContentPane().add(label3);
		pwdFld = new JPasswordField();
		pwdFld.setBounds(110, 105, 110, 22);
		getContentPane().add(pwdFld);

		JLabel label4 = new JLabel("confirm:*") ;
		label4.setBounds(24,144,65,17) ; 
		getContentPane().add(label4) ; 
		pwd2Fld = new JPasswordField();
		pwd2Fld.setBounds(110, 142, 110, 22);
		getContentPane().add(pwd2Fld);

		
		ok = new JButton("提交");
		ok.setBounds(100, 176, 60, 28);
		getContentPane().add(ok);

		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/////////////////////事件监听器///////////////////////

		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				RegisteFrame.this.dispose();
			}
		});

		ok.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				registe() ; 
			}
		});
	}

	
	//****************************************
	private void registe(){
		StringBuffer sb = new StringBuffer("registe ") ;
		sb.append(this.username.getText()+" ")
			.append(this.nickname.getText()+" ")
			.append(this.pwdFld.getText()+" ") ;
	
		
		System.out.println("regist:") ;
		System.out.println(sb.toString()) ; /// 
		
		//发送请求。
		/*
		String response = (String) Common.commandProxy.send(sb.toString()) ; 
		System.out.println(response) ;
		
		if( response.equals("SUCCEED")){
			System.out.println("注册成功") ;
			this.dispose(); 
			new LoginFrame() ; 
		}else if( response.equals("USERNAME_EXIST") ){
			System.out.println("已存在用户名");
		} else {
			System.out.println("其他错误") ; 
		}
		*/
		
		Common.commandProxy.send(sb.toString()); 
	}

}
