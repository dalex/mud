package dalex.client;
import java.awt.TextArea;
import java.io.*;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;

import dalex.command.*;

public class Common {
	public static Map<String,Command> COMMAND = new HashMap<String,Command>();
	
	public static final String SERVER = "127.0.0.1";
	public static final int SERVER_PORT = 7890;
	
	public static Socket socket ;
	static ObjectOutputStream out ; 
	public static CommandProxy commandProxy; 
	
	public static TextArea textArea = null ; 
	
	
	public static LoginFrame loginFrame ;  
	public static RegisteFrame registeFrame ; 
	
	public static String nickname ;
	
	public static void init(){
		COMMAND.put("fight", new FightCommand());
		COMMAND.put("registe",new RegistCommand()) ; 
		COMMAND.put("login", new LoginCommand()) ;
		COMMAND.put("logout",new LogoutCommand()) ; 
		COMMAND.put("talkTo",new TalkCommand()) ; 
		COMMAND.put("broadcast", new BroadcastCommand()) ; 
		COMMAND.put("look", new LookCommand()) ; 
		
		COMMAND.put("south", new SouthCommand());
		COMMAND.put("north", new NorthCommand());
		COMMAND.put("east", new EastCommand());
		COMMAND.put("west", new WestCommand());
		
		//s n e w have the same func
		COMMAND.put("s", new SouthCommand());
		COMMAND.put("n", new NorthCommand());
		COMMAND.put("e", new EastCommand());
		COMMAND.put("w", new WestCommand());
	}
}
