package dalex.world;

public class Position {
	private int x;
	private int y;
	private int mapID;
	
	public Position(int x,int y,int mapID){
		this.mapID = mapID;
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Position pos){
		if(mapID == pos.getMapID()
				&& x == pos.getX() && y == pos.getY()){
			return true;
		}else{
			return false;
		}
	}
	
	public int getMapID(){
		return mapID;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
}
