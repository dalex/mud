package dalex.world;

import java.util.HashMap;
import dalex.player.Monster;
import dalex.player.Player;

public class Site {
	private String name;
	private String description;
	private Position pos;
	HashMap<String,NPC> NPCMap;
	HashMap<String,Monster> monsterMap;
	HashMap<String,Player> playerMap;
	
	public Site(String name,Position pos){
		this.name = name;
		this.pos = pos;
	}
	
	public void setDescription(String des){
		description = des;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String getName(){
		return name;
	}
	
	public Position getPosition(){
		return pos;
	}
	
	public void addNPC(String name,NPC npc){
		NPCMap.put(name, npc);
	}
	
	public void addMonster(String name,Monster monster){
		monsterMap.put(name, monster);
	}
	
	public void addPlayer(String name,Player player){
		playerMap.put(name, player);
	}
	
	public void removePlayer(String name){
		playerMap.remove(name);
	}
}
