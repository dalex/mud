package dalex.world;

import java.io.File;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import dalex.player.Monster;
import dalex.player.Player;

import dalex.server.Common;

public class World {
	private static World theWorld;
	public static final String ROOT = "res";
	public static final String FILE = "config.xml";
	private Map[][] world;
	
	private World(){
		try{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			WorldHandler handler = new WorldHandler();
			parser.parse(new File(ROOT + "/" + FILE), handler);
			world = handler.getWorld();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Map getMap(int x,int y){
		return world[x][y];
	}
	
	public Position getPosition(String name){
		for(int i = 0 ; i < world.length ; i++){
			for(int j = 0; j < world[i].length ; j++){
				if(world[i][j].getName().equals(name)){
					return world[i][j].getMapPosition();
				}
			}
		}
		return new Position(-1,-1,-1);
	}
	
	public Map getMap(Position pos){
		for(int i = 0 ; i < world.length ; i++){
			for(int j = 0; j < world[i].length ; j++){
				if(world[i][j].getMapPosition().getMapID() == pos.getMapID()){
					return world[i][j];
				}
			}
		}
		return null;
	}
	
	public static World createWorld(){
		if(theWorld == null){
			theWorld = new World();
			return theWorld;
		}else{
			return theWorld;
		}
	}
	
}

class WorldHandler extends DefaultHandler{
	private Stack<String> stack = new Stack<String>();
	private String type;
	private String name;
	private String value;
	private Map[][] world;
	
	public Map[][] getWorld(){
		return world;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException{
		stack.push(qName);
		
		for(int i = 0; i < attributes.getLength(); i++){
			String attrName = attributes.getQName(i);
			String attrValue = attributes.getValue(i);
			System.out.println(attrName + "=" + attrValue);
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException{
		String tag = stack.peek();
		
		if("type".equals(tag)){
			type = new String(ch, start,length);
		}else if("name".equals(tag)){
			name = new String(ch, start, length);
		}else if("value".equals(tag)){
			value = new String(ch, start, length);
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		stack.pop(); //表示该元素已经解析完毕，需要从栈中弹出
		
		if("node".equals(qName)){
			System.out.println("type：" + type);
			System.out.println("name：" + name);
			System.out.println("value：" + value);
			System.out.println();
			
			if(type.equals("size")){
				int x = Integer.parseInt(value.split(",")[0]);
				int y = Integer.parseInt(value.split(",")[1]);
				world = new Map[x][y];
				
				for(int i = 0; i < x; i++){
					for(int j = 0; j < y; j++){
						world[i][j] = new Map();
					}
				}
				
			}else if(type.equals("map")){
				Map tempMap = new Map(World.ROOT + "/" + value);
				tempMap.setMap();
				Position pos = tempMap.getMapPosition();
				//System.out.println(tempMap.getName());
				world[pos.getX()][pos.getY()] = tempMap;

			}
		}
		
	}
	
	
}
